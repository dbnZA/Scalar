// <copyright file="ExprModule.fs" company="Jita Project">
// Copyright (c) Jita Project. All rights reserved.
// </copyright>
namespace Jita

open Jita.Implementation

type JitaFormat =
    | FSharp
    | Latex
    | Excel

[<CompilationRepresentation(CompilationRepresentationFlags.ModuleSuffix)>]
module Expr =
    let compileMsil<'a> symbols exprs = exprs |> Msil.compile<'a> symbols

    let deduplicateTree expr = expr |> deduplicateTree

    let deduplicateTrees exprs = exprs |> deduplicateTrees

    let deserializeByteCode stream = deserializeByteCode stream

    let equals expr1 expr2 = ExprComparer.Equals(expr1, expr2)

    let evaluate variables expr = expr |> evaluateTree variables

    let traverse visitor expr = expr |> ExprComparer.Traverse visitor

    let traverseFast visitor expr =
        expr |> ExprComparer.TraverseFast visitor

    let format style expr =
        match style with
        | FSharp -> expr |> fsharp
        | Latex -> expr |> latex
        | Excel -> expr |> excel

    let serializeByteCode stream options expr =
        expr |> serializeByteCode stream options
