// <copyright file="Representation.fs" company="Jita Project">
// Copyright (c) Jita Project. All rights reserved.
// </copyright>
namespace Jita.Implementation

open System
open System.Text
open Jita

[<AutoOpen>]
module internal Format =
    [<Literal>]
    let private LevelRequired = Int32.MinValue

    [<Literal>]
    let private LevelDefault = 0

    [<Literal>]
    let private Level1 = 1

    [<Literal>]
    let private Level2 = 2

    [<Literal>]
    let private Level3 = 3

    [<Literal>]
    let private LevelMax = 4

    type private Bracket =
        { Open: string
          Close: string
          Level: int }

    let private noBracket =
        { Open = ""
          Close = ""
          Level = LevelDefault }

    let private bracketDefault =
        { Open = "("
          Close = ")"
          Level = LevelDefault }

    let private bracketRequired =
        { bracketDefault with
            Level = LevelRequired }

    let private bracket1 = { bracketDefault with Level = Level1 }
    let private bracket2 = { bracketDefault with Level = Level2 }
    let private bracket3 = { bracketDefault with Level = Level3 }
    let private bracketMax = { bracketDefault with Level = LevelMax }

    type private ExprStringBuilder =
        | Empty
        | Open of Bracket
        | Close of Bracket
        | Text of string
        | Value of float
        | Expression of Expr

    type private IVisitor =
        abstract VisitExpr: Expr -> ExprStringBuilder list

    let private get =
        function
        | NonInverse e -> e
        | Inverse e -> e

    let private (|ExpandProduct|_|) apply =
        function
        | Product(c, s) ->
            s
            |> List.rev
            |> fun s ->
                match c, s with
                | 1., (Inverse _ :: _ as s) -> NonInverse(Constant 1.) :: s
                | 1., s -> s
                | c, s -> NonInverse(Constant c) :: s
            |> List.map apply
            |> Some
        | _ -> None

    let private (|ExpandSum|_|) apply =
        function
        | Sum(c, s) ->
            s
            |> List.rev
            |> fun s ->
                match c, s with
                | 0., Inverse e :: s -> NonInverse(UnaryFunction(Negate, e)) :: s
                | 0., s -> s
                | c, s -> NonInverse(Constant c) :: s
            |> List.map apply
            |> Some
        | _ -> None

    let private traverse (visitor: IVisitor) (prefix: string) expr =
        let rec traverse' exprs levels (repr: StringBuilder) =
            match exprs, levels with
            | Empty :: exprs, _ -> traverse' exprs levels repr
            | Open bracket :: exprs, level :: _ ->
                if level > bracket.Level then
                    traverse' ((Text bracket.Open) :: exprs) (bracket.Level :: levels) repr
                else
                    traverse' exprs (bracket.Level :: levels) repr
            | Close bracket :: _, level :: _ when bracket.Level <> level -> failwith "Unbalanced brackets"
            | Close bracket :: exprs, _ :: (level :: _ as levels) ->
                if level > bracket.Level then
                    traverse' ((Text bracket.Close) :: exprs) levels repr
                else
                    traverse' exprs levels repr
            | Text s :: exprs, _ -> traverse' exprs levels (repr.Append(s))
            | Value c :: exprs, _ -> traverse' exprs levels (repr.Append(c))
            | Expression e :: exprs, _ -> traverse' ((visitor.VisitExpr e) @ exprs) levels repr
            | [], [ _ ] -> repr.ToString()
            | _ -> failwith "Unbalanced brackets"

        traverse' [ Expression expr ] [ LevelDefault ] (StringBuilder(prefix))

    let private applyOp bracket (prefix, innerBracket) es =
        let rec apply' exprs =
            function
            | (s, e, b) :: (_ :: _ as es) -> apply' ((Text s) :: (Open b) :: (Expression e) :: (Close b) :: exprs) es
            | [ _, e, b ] -> apply' ((Open b) :: (Expression e) :: (Close b) :: exprs) []
            | [] -> (Open bracket) :: (Text prefix) :: (Open innerBracket) :: exprs

        es |> List.rev |> apply' [ Close innerBracket; Close bracket ]

    let fsharp expr =
        let apply1 bracket prefix e =
            applyOp bracket (prefix, noBracket) [ "", e, bracketMax ]

        let apply2 bracket prefix (e1, b1) sep (e2, b2) =
            applyOp bracket (prefix, noBracket) [ sep, e1, b1; sep, e2, b2 ]

        let infix bracket e1 op e2 = apply2 bracket "" e1 op e2
        let func name e = apply1 bracket3 $"{name} " e

        let func2 name e1 e2 =
            apply2 bracket3 $"{name} " (e1, bracketMax) " " (e2, bracketMax)

        let mapSum =
            function
            | NonInverse e -> " + ", e, bracket1
            | Inverse e -> " - ", e, bracket1

        let mapProduct =
            function
            | NonInverse e -> " * ", e, bracket2
            | Inverse e -> " / ", e, bracket3

        ("", expr)
        ||> traverse
                { new IVisitor with
                    member _.VisitExpr e =
                        match e |> ExprComparer.Unwrap with
                        | BinaryFunction(op, e1, e2) ->
                            match op with
                            | Add -> infix bracket1 (e1, bracket1) " + " (e2, bracket1)
                            | CopySign -> func2 "copysign" e1 e2
                            | Divide -> infix bracket1 (e1, bracket1) " / " (e2, bracket3)
                            | Hypotenuse -> func2 "hypot" e1 e2
                            | Max -> func2 "max" e1 e2
                            | Min -> func2 "min" e1 e2
                            | Modulus -> infix bracket2 (e1, bracket2) " % " (e2, bracket2)
                            | Multiply -> infix bracket2 (e1, bracket2) " * " (e2, bracket2)
                            | Power -> infix bracket3 (e1, bracketMax) " ** " (e2, bracket3)
                            | Subtract -> infix bracket1 (e1, bracket1) " - " (e2, bracket1)
                        | Constant c -> [ Value c ]
                        | ExpandProduct mapProduct s -> applyOp bracket2 ("", noBracket) s
                        | Root(e, n) -> func2 "rootn" e (Constant n)
                        | ExpandSum mapSum s -> applyOp bracket1 ("", noBracket) s
                        | UnaryFunction(op, e) ->
                            match op with
                            | Absolute -> func "abs" e
                            | CubeRoot -> func "cbrt" e
                            | Exponent -> func "exp" e
                            | Logarithm -> func "log" e
                            | Negate -> apply1 bracketMax "-" e
                            | SquareRoot -> func "sqrt" e
                        | Variable v -> [ Text v ]
                        | _ -> failwith $"Unexpected expression after unwrapping: {e}" }

    let latex expr =
        let group =
            { Open = "{"
              Close = "}"
              Level = LevelMax }

        let groupRequired = { group with Level = LevelRequired }

        let barRequired =
            { Open = @"\left|"
              Close = @"\right|"
              Level = LevelRequired }

        let bracketDefault =
            { Open = @"\left("
              Close = @"\right)"
              Level = LevelDefault }

        let bracket1 = { bracketDefault with Level = Level1 }
        let bracket2 = { bracketDefault with Level = Level2 }
        let bracket3 = { bracketDefault with Level = Level3 }
        let bracketMax = { bracketDefault with Level = LevelMax }

        let apply1 bracket prefix e =
            applyOp bracket (prefix, noBracket) [ "", e, bracketMax ]

        let apply2 bracket prefix (e1, b1) sep (e2, b2) =
            applyOp bracket (prefix, noBracket) [ sep, e1, b1; sep, e2, b2 ]

        let infix bracket e1 op e2 = apply2 bracket "" e1 op e2

        let func name (e, b) =
            applyOp group (name, noBracket) [ "", e, b ]

        let func2 name e1 e2 =
            applyOp group (name, bracket3) [ ",", e1, noBracket; ",", e2, noBracket ]

        let mapProduct =
            function
            | NonInverse e -> "\cdot ", e, bracket2
            | Inverse e -> "/", e, bracket3

        let mapSum =
            function
            | NonInverse e -> "+", e, bracket1
            | Inverse e -> "-", e, bracket1

        ("", expr)
        ||> traverse
                { new IVisitor with
                    member _.VisitExpr e =
                        match e |> ExprComparer.Unwrap with
                        | BinaryFunction(op, e1, e2) ->
                            match op with
                            | Add -> infix bracket1 (e1, bracket1) "+" (e2, bracket1)
                            | CopySign -> func2 "copysign" e1 e2
                            | Divide ->
                                applyOp group (@"\frac", noBracket) [ "", e1, groupRequired; "", e2, groupRequired ]
                            | Hypotenuse -> [ Expression(sqrt (e1 ** Constant 2. + e2 ** Constant 2.)) ]
                            | Max -> func2 "max" e1 e2
                            | Min -> func2 "min" e1 e2
                            | Modulus -> infix bracket2 (e1, bracket2) "\mod " (e2, bracket2)
                            | Multiply -> infix bracket2 (e1, bracket2) "\cdot " (e2, bracket2)
                            | Power -> infix bracket3 (e1, bracket3) "^" (e2, groupRequired)
                            | Subtract -> infix bracket1 (e1, bracket1) "-" (e2, bracket1)
                        | Constant c -> [ Value c ]
                        | ExpandProduct mapProduct s -> applyOp bracket2 ("", noBracket) s
                        | Root(e, n) -> func $"\sqrt[{n}]" (e, groupRequired)
                        | ExpandSum mapSum s -> applyOp bracket1 ("", noBracket) s
                        | UnaryFunction(op, e) ->
                            match op with
                            | Absolute -> applyOp group ("", barRequired) [ "", e, noBracket ]
                            | CubeRoot -> func "\sqrt[3]" (e, groupRequired)
                            | Exponent -> func "\mathrm{e}^" (e, groupRequired)
                            | Logarithm -> func "\log " (e, bracket3)
                            | Negate -> apply1 bracketMax "-" e
                            | SquareRoot -> func "\sqrt" (e, groupRequired)
                        | Variable v -> [ Text v ]
                        | _ -> failwith "Unexpected expression after unwrapping" }

    let excel expr =
        let infix bracket e1 sep e2 =
            applyOp bracket ("", noBracket) [ sep, e1, bracket; sep, e2, bracket ]

        let func name e =
            applyOp noBracket (name, bracketRequired) [ "", e, noBracket ]

        let func2 name e1 e2 =
            applyOp noBracket (name, bracketRequired) [ ",", e1, noBracket; ",", e2, noBracket ]

        let mapProduct =
            function
            | NonInverse e -> "*", e, bracket2
            | Inverse e -> "/", e, bracket3

        let mapSum =
            function
            | NonInverse e -> "+", e, bracket1
            | Inverse e -> "-", e, bracket1

        ("=", expr)
        ||> traverse
                { new IVisitor with
                    member _.VisitExpr e =
                        match e |> ExprComparer.Unwrap with
                        | BinaryFunction(op, e1, e2) ->
                            match op with
                            | Add -> infix bracket1 e1 "+" e2
                            | CopySign -> func "ABS" e1 @ [ Text "*" ] @ func "SIGN" e2
                            | Divide -> infix bracket1 e1 "/" e2
                            | Hypotenuse -> [ Expression(sqrt (e1 ** Constant 2. + e2 ** Constant 2.)) ]
                            | Max -> func2 "MAX" e1 e2
                            | Min -> func2 "MIN" e1 e2
                            | Modulus -> func2 "MOD" e1 e2
                            | Multiply -> infix bracket2 e1 "*" e
                            | Power -> func2 "POW" e1 e2
                            | Subtract -> infix bracket1 e1 "-" e2
                        | Constant c -> [ Value c ]
                        | ExpandProduct mapProduct s -> applyOp bracket2 ("", noBracket) s
                        | Root(e, n) -> [ Expression(e ** (Expr.One / Constant n)) ]
                        | ExpandSum mapSum s -> applyOp bracket1 ("", noBracket) s
                        | UnaryFunction(op, e) ->
                            match op with
                            | Absolute -> func "ABS" e
                            | CubeRoot -> [ Expression(e ** (Expr.One / Constant 3.)) ]
                            | Exponent -> func "EXP" e
                            | Logarithm -> func "LOG" e
                            | Negate -> applyOp noBracket ("-", noBracket) [ "", e, bracket2 ]
                            | SquareRoot -> func "SQRT" e
                        | Variable v -> [ Text v ]
                        | _ -> failwith "Unexpected expression after unwrapping" }
