// <copyright file="Evaluate.fs" company="Jita Project">
// Copyright (c) Jita Project. All rights reserved.
// </copyright>
namespace Jita.Implementation

open Jita
open Jita.Math

[<AutoOpen>]
module internal Evaluate =
    let evaluateTree variables expr =
        expr
        |> ExprComparer.TraverseFast
            { new SimpleExprVisitor<float>() with
                member _.VisitBinaryFunc op v1 v2 =
                    match op with
                    | Add -> v1 + v2
                    | CopySign -> copysign v1 v2
                    | Divide -> v1 / v2
                    | Hypotenuse -> hypot v1 v2
                    | Max -> max v1 v2
                    | Min -> min v1 v2
                    | Modulus -> v1 % v2
                    | Multiply -> v1 * v2
                    | Power -> v1 ** v2
                    | Subtract -> v1 - v2

                member _.VisitConstant value = value

                member _.VisitRoot v n = rootn v n

                member _.VisitUnaryFunc op value =
                    match op with
                    | Absolute -> abs value
                    | CubeRoot -> cbrt (value)
                    | Exponent -> exp value
                    | Logarithm -> log value
                    | Negate -> -value
                    | SquareRoot -> sqrt value

                member _.VisitVariable name =
                    variables |> Map.tryFind name |> Option.defaultValue nan }
