// <copyright file="JitaSerializationOption.fs" company="Jita Project">
// Copyright (c) Jita Project. All rights reserved.
// </copyright>
namespace Jita

type JitaSerializationOption =
    | Exact
    | Compressed
