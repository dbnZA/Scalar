// <copyright file="Expr.fs" company="Jita Project">
// Copyright (c) Jita Project. All rights reserved.
// </copyright>
namespace Jita

open Jita.Implementation

[<CompilationRepresentation(CompilationRepresentationFlags.ModuleSuffix)>]
module Expr =
    let compileLlvm<'a> symbols exprs = exprs |> Llvm.compile<'a> symbols
