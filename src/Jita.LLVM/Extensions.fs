// <copyright file="Extensions" company="Jita Project">
// Copyright (c) Jita Project. All rights reserved.
// </copyright>
namespace Jita

open System
open System.Runtime.CompilerServices
open Jita.Implementation

[<AbstractClass; Sealed>]
[<Extension>]
type Extensions =
    [<Extension>]
    static member CompileLlvm<'T>(this, [<ParamArray>] variables) = this |> Llvm.compile<'T> variables
