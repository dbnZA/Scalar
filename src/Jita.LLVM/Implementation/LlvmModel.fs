// <copyright file="LLVMModel.fs" company="Jita Project">
// Copyright (c) Jita Project. All rights reserved.
// </copyright>
namespace Jita.Implementation

open System
open System.Numerics
open LLVMSharp.Interop

type private LlvmModel =
    { Abs: string
      Cbrt: string
      CopySign: string
      Exp: string
      Hypot: string
      Log: string
      Sqrt: string
      Pow: string
      LLVMType: LLVMTypeRef
      LLVMBaseType: LLVMTypeRef
      Alignment: int
      VectorCount: int }

[<CompilationRepresentation(CompilationRepresentationFlags.ModuleSuffix)>]
module private LlvmModel =
    let private getScalar<'a> () =
        if typeof<'a> = typeof<float> then
            { Abs = "fabs"
              Cbrt = "cbrt"
              CopySign = "copysign"
              Exp = "exp"
              Hypot = "hypot"
              Log = "log"
              Sqrt = "sqrt"
              Pow = "pow"
              LLVMType = LLVMTypeRef.Double
              LLVMBaseType = LLVMTypeRef.Double
              Alignment = 1
              VectorCount = 1 }
        elif typeof<'a> = typeof<float32> then
            { Abs = "fabsf"
              Cbrt = "cbrtf"
              CopySign = "copysignf"
              Exp = "expf"
              Hypot = "hypotf"
              Log = "logf"
              Sqrt = "sqrtf"
              Pow = "powf"
              LLVMType = LLVMTypeRef.Float
              LLVMBaseType = LLVMTypeRef.Float
              Alignment = 1
              VectorCount = 1 }
        else
            failwith $"LLVM: unsupported type: {typeof<'a>.Name}"

    let private getVector<'a when 'a: (new: unit -> 'a) and 'a: struct and 'a :> ValueType> () =
        let scalarModel = getScalar<'a> ()

        { scalarModel with
            LLVMType = LLVMTypeRef.CreateVector(scalarModel.LLVMType, Vector<'a>.Count |> uint32)
            Alignment = 16
            VectorCount = Vector<'a>.Count }

    let get<'a> () =
        if typeof<'a> = typeof<float Vector> then
            getVector<float> ()
        elif typeof<'a> = typeof<float32 Vector> then
            getVector<float32> ()
        else
            getScalar<'a> ()
