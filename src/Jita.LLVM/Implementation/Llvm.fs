// <copyright file="LLVM.fs" company="Jita Project">
// Copyright (c) Jita Project. All rights reserved.
// </copyright>
namespace Jita.Implementation

#nowarn "25"

open System
open System.Collections.Generic
open Jita
open LLVMSharp.Interop

module internal Llvm =
    [<Literal>]
    let private ADDRESS_SPACE_GLOBAL = 0u

    type private OpType =
        | MaxOp
        | MinOp

    let private createConstInt value =
        LLVMValueRef.CreateConstInt(LLVMTypeRef.Int32, value |> uint64)

    let private getElementPtr ty ptr index (builder: LLVMBuilderRef) =
        builder.BuildGEP2(ty, ptr, [| createConstInt index |])

    type private LLVMVisitor(jita: LLVMModuleRef, func: LLVMValueRef, builder: LLVMBuilderRef, model, findVariable) =
        inherit SimpleExprVisitor<LLVMValueRef * bool>()

        let (=.) (e1, _) (e2, _) =
            builder.BuildFCmp(LLVMRealPredicate.LLVMRealOEQ, e1, e2)

        let (=~) e1 e2 =
            builder.BuildICmp(LLVMIntPredicate.LLVMIntEQ, e1, e2)

        let (<>.) (e1, _) (e2, _) =
            builder.BuildFCmp(LLVMRealPredicate.LLVMRealONE, e1, e2)

        let (<.) (e1, _) (e2, _) =
            builder.BuildFCmp(LLVMRealPredicate.LLVMRealOLT, e1, e2)

        let (<=.) (e1, _) (e2, _) =
            builder.BuildFCmp(LLVMRealPredicate.LLVMRealOLE, e1, e2)

        let (>.) (e1, _) (e2, _) =
            builder.BuildFCmp(LLVMRealPredicate.LLVMRealOGT, e1, e2)

        let (>=.) (e1, _) (e2, _) =
            builder.BuildFCmp(LLVMRealPredicate.LLVMRealOGE, e1, e2)

        let (&&.) e1 e2 = builder.BuildAnd(e1, e2)

        let ``if`` cmp (thenValue, _) (elseValue, _) =
            let thenBlock = func.AppendBasicBlock("then")
            let elseBlock = func.AppendBasicBlock("else")
            let mergeBlock = func.AppendBasicBlock("merge")

            builder.BuildCondBr(cmp, thenBlock, elseBlock) |> ignore

            for block in [ thenBlock; elseBlock ] do
                builder.PositionAtEnd(block)
                builder.BuildBr(mergeBlock) |> ignore

            builder.PositionAtEnd(mergeBlock)
            let phi = builder.BuildPhi(model.LLVMBaseType)

            phi.AddIncoming([| thenValue; elseValue |], [| thenBlock; elseBlock |], 2u)
            phi, true

        let iter args predicate =
            if model.VectorCount = 1 then
                predicate args
            else
                let rec iter index values =
                    if index = model.VectorCount then
                        values, true
                    else
                        let i = createConstInt index

                        let value, _ =
                            let args =
                                args
                                |> Array.map (fun (arg, status) -> builder.BuildExtractElement(arg, i), status)

                            predicate args

                        iter (index + 1) (builder.BuildInsertElement(values, value, i))

                iter 0 (LLVMValueRef.CreateConstReal(model.LLVMType, 0.))

        let doMaxMin op v1 v2 =
            iter [| v1; v2 |] (fun [| v1; v2 |] ->
                match op with
                | MaxOp -> ``if`` (v1 <. v2) v2 v1
                | MinOp -> ``if`` (v1 <. v2) v1 v2)

        let doCall (func, funcType) args =
            builder.BuildCall2(funcType, func, Array.map fst args), true

        let callFunc func args = iter args (doCall func)

        let createFunction args (name: string) =
            let funcType =
                LLVMTypeRef.CreateFunction(model.LLVMBaseType, Array.create args model.LLVMBaseType, false)

            jita.AddFunction(name, funcType, Linkage = LLVMLinkage.LLVMExternalLinkage), funcType

        let func1 = createFunction 1

        let func2 = createFunction 2

        let abs = func1 model.Abs

        let cbrt = func1 model.Cbrt

        let copysign = func2 model.CopySign

        let exp = func1 model.Exp

        let hypot = func2 model.Hypot

        let log = func1 model.Log

        let sqrt = func1 model.Sqrt

        let pow = func2 model.Pow

        let ``0.`` = LLVMValueRef.CreateConstReal(model.LLVMBaseType, 0.), false

        override _.VisitBinaryFunc op (e1, _ as v1) (e2, _ as v2) =
            match op with
            | Add -> builder.BuildFAdd(e1, e2), true
            | CopySign -> callFunc copysign [| v1; v2 |]
            | Hypotenuse -> callFunc hypot [| v1; v2 |]
            | Divide -> builder.BuildFDiv(e1, e2), true
            | Max -> doMaxMin MaxOp v1 v2
            | Min -> doMaxMin MinOp v1 v2
            | Modulus -> builder.BuildFRem(e1, e2), true
            | Multiply -> builder.BuildFMul(e1, e2), true
            | Power -> callFunc pow [| v1; v2 |]
            | Subtract -> builder.BuildFSub(e1, e2), true

        override _.VisitConstant c =
            LLVMValueRef.CreateConstReal(model.LLVMType, c), false

        override this.VisitRoot v n =
            let inline ``const`` x =
                LLVMValueRef.CreateConstReal(model.LLVMBaseType, x), false

            let genericRoot lowerBound upperBound =
                iter [| v |] (fun ([| v |] as arg) ->
                    let absV = doCall abs arg

                    ``if``
                        ((v =. v) &&. (absV <>. ``const`` infinity))
                        (``if``
                            (v =. ``0.``)
                            (if Int32.IsOddInteger(n) then
                                 doCall copysign [| ``const`` upperBound; v |]
                             else
                                 ``const`` upperBound)
                            (if Int32.IsOddInteger(n) then
                                 doCall copysign [| doCall pow [| absV; ``const`` (1. / float n) |]; v |]
                             else
                                 ``if`` (v >. ``0.``) (doCall pow [| v; ``const`` (1. / float n) |]) (``const`` nan)))
                        (``if``
                            (v <=. ``0.``)
                            (``const`` (if Int32.IsOddInteger(n) then lowerBound else nan))
                            (``if`` (v >=. ``0.``) (``const`` -lowerBound) (``const`` nan))))

            match n with
            | 0 -> this.VisitConstant nan
            | 1 -> v
            | 2 -> iter [| v |] (fun ([| v |] as arg) -> ``if`` (v =. ``0.``) ``0.`` (doCall sqrt arg))
            | 3 -> this.VisitUnaryFunc CubeRoot v
            | n when n > 0 -> genericRoot -infinity 0.
            | _ -> genericRoot -0. infinity

        override _.VisitUnaryFunc op (e, _ as v) =
            match op with
            | Absolute -> callFunc abs [| v |]
            | CubeRoot -> callFunc cbrt [| v |]
            | Exponent -> callFunc exp [| v |]
            | Logarithm -> callFunc log [| v |]
            | Negate -> builder.BuildFNeg(e), true
            | SquareRoot -> callFunc sqrt [| v |]

        override _.VisitVariable v =
            builder.BuildLoad2(
                model.LLVMType,
                builder
                |> getElementPtr model.LLVMType func.Params[Compile.InputArg] (findVariable v)
            ),
            false

    let mutable llvmInitialized = false

    let private llvmCompileLock = obj ()

    let compile<'a> variables exprs =
        let inputLength = variables |> Array.length
        let outputLength = exprs |> Array.length

        if outputLength = 0 then
            invalidArg (nameof exprs) $"At least one {nameof Expr} must be specified"

        let exprComparer = ExprComparer(exprs)
        let localExpr, localLength = exprs |> Compile.markToLocal exprComparer
        let cachedExpr = HashSet<Expr>(exprComparer)

        lock llvmCompileLock (fun _ ->
            let model = LlvmModel.get<'a> ()
            let jita = LLVMModuleRef.CreateWithName("Jita")
            let doublePtrType = LLVMTypeRef.CreatePointer(model.LLVMType, ADDRESS_SPACE_GLOBAL)
            let paramsType = [| doublePtrType; doublePtrType; doublePtrType |]

            let func =
                jita.AddFunction("JitaLLVM", LLVMTypeRef.CreateFunction(LLVMTypeRef.Void, paramsType))

            use builder = jita.Context.CreateBuilder()
            let entry = func.AppendBasicBlock("entry")
            builder.PositionAtEnd(entry)

            let llvmVisitor =
                { Callback = LLVMVisitor(jita, func, builder, model, Compile.findVariable variables)
                  LeadFilter =
                    fun expr ->
                        if cachedExpr.Contains(expr) then
                            let valueRef =
                                builder.BuildLoad2(
                                    model.LLVMType,
                                    builder
                                    |> getElementPtr model.LLVMType func.Params[Compile.LocalArg] localExpr[expr]
                                )

                            Some(valueRef, false)
                        else
                            None
                  TailFilter =
                    fun expr (valueRef, tryCache) ->
                        if tryCache then
                            let isLocal, index = localExpr.TryGetValue(expr)

                            if isLocal then
                                builder.BuildStore(
                                    valueRef,
                                    builder |> getElementPtr model.LLVMType func.Params[Compile.LocalArg] index
                                )
                                |> ignore

                            cachedExpr.Add(expr) |> ignore

                        valueRef, false }

            exprs
            |> Array.iteri (fun i expr ->
                let result = expr |> Expr.traverse llvmVisitor

                builder.BuildStore(
                    result |> fst,
                    builder |> getElementPtr model.LLVMType func.Params[Compile.OutputArg] i
                )
                |> ignore)

            builder.BuildRetVoid() |> ignore

            jita.Verify(LLVMVerifierFailureAction.LLVMPrintMessageAction)

            if not llvmInitialized then
                LLVM.LinkInMCJIT()

                if LLVM.InitializeNativeTarget() <> 0 then
                    failwith "LLVM failed to initialise native target"

                if LLVM.InitializeNativeAsmParser() <> 0 then
                    failwith "LLVM failed to initialise native assembler parser"

                if LLVM.InitializeNativeAsmPrinter() <> 0 then
                    failwith "LLVM failed to initialise native assembler printer"

                llvmInitialized <- true

            let mutable options =
                LLVMMCJITCompilerOptions.Create(NoFramePointerElim = 1, EnableFastISel = 1)

            let func = jita.CreateMCJITCompiler(&options).GetPointerToGlobal<Compile.Func>(func)
            ExprFunc<'a>(model.Alignment, inputLength, outputLength, localLength, func))
