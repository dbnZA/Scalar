// <copyright file="Format.fs" company="Jita Project">
// Copyright (c) Jita Project. All rights reserved.
// </copyright>
namespace Jita

open System.Globalization
open NUnit.Framework
open FsCheck
open FsCheck.NUnit
open Jita.Math

[<SetUpFixture>]
type RegisterMathModels() =
    [<OneTimeSetUp>]
    member _.InvariantCulture() =
        let culture = CultureInfo.InvariantCulture
        CultureInfo.CurrentCulture <- culture

module FormatTests =
    let x = Variable "x"

    let y = Variable "y"

    let z = Variable "z"

    [<Property(MaxTest = 1)>]
    let ``format fsharp expr`` () =
        [ x + y, "x + y"
          x / y, "x / y"
          max x y, "max x y"
          min x y, "min x y"
          x * y, "x * y"
          x ** y, "x ** y"
          x - y, "x - y"
          x % y, "x % y"
          copysign x y, "copysign x y"
          rootn x 8, "rootn x 8"
          Constant 2., "2"
          Constant 0.5, "0.5"
          abs x, "abs x"
          exp x, "exp x"
          log x, "log x"
          -x, "-x"
          sqrt x, "sqrt x"
          x, "x"
          y, "y" ]
        |> Seq.map (fun (expr, value) -> expr |> Expr.format FSharp |> equals value)
        |> Seq.reduce (.&.)

    [<Property(MaxTest = 1)>]
    let ``format fsharp compound`` () =
        [ x * z + y * z, "x * z + y * z"
          (x + z) / (y + z), "(x + z) / (y + z)"
          max (x + z) (y + z), "max (x + z) (y + z)"
          min (x + z) (y + z), "min (x + z) (y + z)"
          (x + z) * (y + z), "(x + z) * (y + z)"
          x * z ** y * z, "x * z ** y * z"
          (x * z) ** (y * z), "(x * z) ** (y * z)"
          x ** y ** z, "x ** y ** z"
          (x ** y) ** z, "(x ** y) ** z"
          x * z - y * z, "x * z - y * z"
          x * z % y * z, "x * z % y * z"
          copysign (x * z) (y * z), "copysign (x * z) (y * z)"
          Constant 2., "2"
          Constant 0.5, "0.5"
          abs (x + z), "abs (x + z)"
          exp (x + z), "exp (x + z)"
          log (x + z), "log (x + z)"
          -(x * z), "-(x * z)"
          -(x + z), "-x - z"
          sqrt (x + z), "sqrt (x + z)"
          x + z, "x + z"
          y + z, "y + z" ]
        |> Seq.map (fun (expr, value) -> expr |> Expr.format FSharp |> equals value)
        |> Seq.reduce (.&.)

    [<Property(MaxTest = 1)>]
    let ``format fsharp level 1 context`` () =
        [ z + x + y, "z + x + y"
          z + x / y, "z + x / y"
          z + max x y, "z + max x y"
          z + min x y, "z + min x y"
          z + x * y, "z + x * y"
          z + x ** y, "z + x ** y"
          z + x - y, "z + x - y"
          z + x % y, "z + x % y"
          copysign (z + x) y, "copysign (z + x) y"
          z + Constant 2., "2 + z"
          z + Constant 0.5, "0.5 + z"
          z + abs x, "z + abs x"
          z + exp x, "z + exp x"
          z + log x, "z + log x"
          z + -x, "z + -x"
          z + sqrt x, "z + sqrt x"
          z + x, "z + x"
          z + y, "z + y" ]
        |> Seq.map (fun (expr, value) -> expr |> Expr.format FSharp |> equals value)
        |> Seq.reduce (.&.)

    [<Property(MaxTest = 1)>]
    let ``format fsharp level 2 context`` () =
        [ z * (x + y), "z * (x + y)"
          z * x / y, "z * x / y"
          z / (x + y), "z / (x + y)"
          z * max x y, "z * max x y"
          z * min x y, "z * min x y"
          z * x * y, "z * x * y"
          z * x ** y, "z * x ** y"
          z * (x - y), "z * (x - y)"
          z % (x - y), "z % (x - y)"
          copysign z (x - y), "copysign z (x - y)"
          z * Constant 2., "2 * z"
          z * Constant 0.5, "0.5 * z"
          z * abs x, "z * abs x"
          z * exp x, "z * exp x"
          z * log x, "z * log x"
          z * -x, "-(z * x)"
          z * sqrt x, "z * sqrt x"
          z * x, "z * x"
          z * y, "z * y" ]
        |> Seq.map (fun (expr, value) -> expr |> Expr.format FSharp |> equals value)
        |> Seq.reduce (.&.)

    [<Property(MaxTest = 1)>]
    let ``format fsharp level 3 context`` () =
        [ abs (x + y), "abs (x + y)"
          abs (x / y), "abs (x / y)"
          abs (max x y), "abs (max x y)"
          abs (min x y), "abs (min x y)"
          abs (x * y), "abs (x * y)"
          abs (x ** y), "abs (x ** y)"
          abs (x - y), "abs (x - y)"
          abs (x % y), "abs (x % y)"
          abs (copysign x y), "abs (copysign x y)"
          abs (Constant 2.), "2"
          abs (Constant 0.5), "0.5"
          abs (abs x), "abs (abs x)"
          abs (exp x), "abs (exp x)"
          abs (log x), "abs (log x)"
          abs -x, "abs -x"
          abs (sqrt x), "abs (sqrt x)"
          abs x, "abs x"
          abs y, "abs y" ]
        |> Seq.map (fun (expr, value) -> expr |> Expr.format FSharp |> equals value)
        |> Seq.reduce (.&.)

    [<Property(MaxTest = 1)>]
    let ``format latex expr`` () =
        [ x + y, "x+y"
          x / y, "x/y"
          max x y, @"max\left(x,y\right)"
          min x y, @"min\left(x,y\right)"
          x * y, "x\cdot y"
          x ** y, "x^{y}"
          x - y, "x-y"
          x % y, "x\mod y"
          copysign x y, @"copysign\left(x,y\right)"
          hypot x y, @"\sqrt{x^{2}+y^{2}}"
          rootn x 8, @"\sqrt[8]{x}"
          Constant 2., "2"
          Constant 0.5, "0.5"
          abs x, @"\left|x\right|"
          exp x, @"\mathrm{e}^{x}"
          log x, @"\log x"
          -x, "-x"
          sqrt x, @"\sqrt{x}"
          cbrt x, @"\sqrt[3]{x}"
          x, "x"
          y, "y" ]
        |> Seq.map (fun (expr, value) -> expr |> Expr.format Latex |> equals value)
        |> Seq.reduce (.&.)

    [<Property(MaxTest = 1)>]
    let ``format latex compound`` () =
        [ x * z + y * z, "x\cdot z+y\cdot z"
          (x + z) / (y + z), @"\left(x+z\right)/\left(y+z\right)"
          max (x + z) (y + z), @"max\left(x+z,y+z\right)"
          min (x + z) (y + z), @"min\left(x+z,y+z\right)"
          (x + z) * (y + z), @"\left(x+z\right)\cdot \left(y+z\right)"
          x * z ** y * z, "x\cdot z^{y}\cdot z"
          (x * z) ** (y * z), @"\left(x\cdot z\right)^{y\cdot z}"
          x ** y ** z, "x^{y^{z}}"
          (x ** y) ** z, "x^{y}^{z}"
          x * z - y * z, "x\cdot z-y\cdot z"
          x * z % y * z, "x\cdot z\mod y\cdot z"
          copysign (x * z) (y * z), @"copysign\left(x\cdot z,y\cdot z\right)"
          Constant 2., "2"
          Constant 0.5, "0.5"
          abs (x + z), @"\left|x+z\right|"
          exp (x + z), @"\mathrm{e}^{x+z}"
          log (x + z), @"\log \left(x+z\right)"
          -(x * z), @"-\left(x\cdot z\right)"
          -(x + z), @"-x-z"
          sqrt (x + z), @"\sqrt{x+z}"
          x + z, "x+z"
          y + z, "y+z" ]
        |> Seq.map (fun (expr, value) -> expr |> Expr.format Latex |> equals value)
        |> Seq.reduce (.&.)

    [<Property(MaxTest = 1)>]
    let ``format latex level 1 context`` () =
        [ z + x + y, "z+x+y"
          z + x / y, "z+x/y"
          z + max x y, @"z+max\left(x,y\right)"
          z + min x y, @"z+min\left(x,y\right)"
          z + x * y, "z+x\cdot y"
          z + x ** y, "z+x^{y}"
          z + x - y, "z+x-y"
          z + x % y, "z+x\mod y"
          copysign (z + x) y, @"copysign\left(z+x,y\right)"
          z + Constant 2., "2+z"
          z + Constant 0.5, "0.5+z"
          z + abs x, @"z+\left|x\right|"
          z + exp x, @"z+\mathrm{e}^{x}"
          z + log x, @"z+\log x"
          z + -x, "z+-x"
          z + sqrt x, @"z+\sqrt{x}"
          z + x, "z+x"
          z + y, "z+y" ]
        |> Seq.map (fun (expr, value) -> expr |> Expr.format Latex |> equals value)
        |> Seq.reduce (.&.)

    [<Property(MaxTest = 1)>]
    let ``format latex level 2 context`` () =
        [ z * (x + y), @"z\cdot \left(x+y\right)"
          z * x / y, "z\cdot x/y"
          z / (x + y), @"z/\left(x+y\right)"
          z * max x y, @"z\cdot max\left(x,y\right)"
          z * min x y, @"z\cdot min\left(x,y\right)"
          z * x * y, "z\cdot x\cdot y"
          z * x ** y, "z\cdot x^{y}"
          z * (x - y), @"z\cdot \left(x-y\right)"
          z % (x - y), @"z\mod \left(x-y\right)"
          copysign z (x - y), @"copysign\left(z,x-y\right)"
          z * Constant 2., "2\cdot z"
          z * Constant 0.5, "0.5\cdot z"
          z * abs x, @"z\cdot \left|x\right|"
          z * exp x, @"z\cdot \mathrm{e}^{x}"
          z * log x, @"z\cdot \log x"
          z * -x, @"-\left(z\cdot x\right)"
          z * sqrt x, @"z\cdot \sqrt{x}"
          z * x, "z\cdot x"
          z * y, "z\cdot y" ]
        |> Seq.map (fun (expr, value) -> expr |> Expr.format Latex |> equals value)
        |> Seq.reduce (.&.)

    [<Property(MaxTest = 1)>]
    let ``format latex level 3 context`` () =
        [ abs (x + y), @"\left|x+y\right|"
          abs (x / y), @"\left|x/y\right|"
          abs (max x y), @"\left|max\left(x,y\right)\right|"
          abs (min x y), @"\left|min\left(x,y\right)\right|"
          abs (x * y), @"\left|x\cdot y\right|"
          abs (x ** y), @"\left|x^{y}\right|"
          abs (x - y), @"\left|x-y\right|"
          abs (x % y), @"\left|x\mod y\right|"
          abs (copysign x y), @"\left|copysign\left(x,y\right)\right|"
          abs (Constant 2.), "2"
          abs (Constant 0.5), "0.5"
          abs (abs x), @"\left|\left|x\right|\right|"
          abs (exp x), @"\left|\mathrm{e}^{x}\right|"
          abs (log x), @"\left|\log x\right|"
          abs -x, @"\left|-x\right|"
          abs (sqrt x), @"\left|\sqrt{x}\right|"
          abs x, @"\left|x\right|"
          abs y, @"\left|y\right|" ]
        |> Seq.map (fun (expr, value) -> expr |> Expr.format Latex |> equals value)
        |> Seq.reduce (.&.)

    [<Property(MaxTest = 1)>]
    let ``format excel expr`` () =
        [ x + y, "=x+y"
          x / y, "=x/y"
          max x y, "=MAX(x,y)"
          min x y, "=MIN(x,y)"
          x * y, "=x*y"
          x ** y, "=POW(x,y)"
          x - y, "=x-y"
          x % y, "=MOD(x,y)"
          copysign x y, "=ABS(x)*SIGN(y)"
          hypot x y, "=SQRT(POW(x,2)+POW(y,2))"
          rootn x 8, "=POW(x,0.125)"
          Constant 2., "=2"
          Constant 0.5, "=0.5"
          abs x, "=ABS(x)"
          exp x, "=EXP(x)"
          log x, "=LOG(x)"
          -x, "=-x"
          sqrt x, "=SQRT(x)"
          cbrt x, "=POW(x,0.3333333333333333)"
          x, "=x"
          y, "=y" ]
        |> Seq.map (fun (expr, value) -> expr |> Expr.format Excel |> equals value)
        |> Seq.reduce (.&.)

    [<Property(MaxTest = 1)>]
    let ``format excel compound`` () =
        [ x * z + y * z, "=x*z+y*z"
          (x + z) / (y + z), "=(x+z)/(y+z)"
          max (x + z) (y + z), "=MAX(x+z,y+z)"
          min (x + z) (y + z), "=MIN(x+z,y+z)"
          (x + z) * (y + z), "=(x+z)*(y+z)"
          x * z ** y * z, "=x*POW(z,y)*z"
          (x * z) ** (y * z), "=POW(x*z,y*z)"
          x ** y ** z, "=POW(x,POW(y,z))"
          (x ** y) ** z, "=POW(POW(x,y),z)"
          x * z - y * z, "=x*z-y*z"
          x * z % y * z, "=MOD(x*z,y)*z"
          copysign (x * z) (y * z), "=ABS(x*z)*SIGN(y*z)"
          hypot (x * z) (y * z), "=SQRT(POW(x*z,2)+POW(y*z,2))"
          Constant 2., "=2"
          Constant 0.5, "=0.5"
          abs (x + z), "=ABS(x+z)"
          exp (x + z), "=EXP(x+z)"
          log (x + z), "=LOG(x+z)"
          -(x * z), "=-x*z"
          -(x + z), "=-x-z"
          sqrt (x + z), "=SQRT(x+z)"
          cbrt (x + z), "=POW(x+z,0.3333333333333333)"
          x + z, "=x+z"
          y + z, "=y+z" ]
        |> Seq.map (fun (expr, value) -> expr |> Expr.format Excel |> equals value)
        |> Seq.reduce (.&.)

    [<Property(MaxTest = 1)>]
    let ``format excel level 1 context`` () =
        [ z + x + y, "=z+x+y"
          z + x / y, "=z+x/y"
          z + max x y, "=z+MAX(x,y)"
          z + min x y, "=z+MIN(x,y)"
          z + x * y, "=z+x*y"
          z + x ** y, "=z+POW(x,y)"
          z + x - y, "=z+x-y"
          z + x % y, "=z+MOD(x,y)"
          copysign (z + x) y, "=ABS(z+x)*SIGN(y)"
          hypot (z + x) y, "=SQRT(POW(z+x,2)+POW(y,2))"
          z + Constant 2., "=2+z"
          z + Constant 0.5, "=0.5+z"
          z + abs x, "=z+ABS(x)"
          z + exp x, "=z+EXP(x)"
          z + log x, "=z+LOG(x)"
          z + -x, "=z+-x"
          z + sqrt x, "=z+SQRT(x)"
          z + cbrt x, "=z+POW(x,0.3333333333333333)"
          z + x, "=z+x"
          z + y, "=z+y" ]
        |> Seq.map (fun (expr, value) -> expr |> Expr.format Excel |> equals value)
        |> Seq.reduce (.&.)

    [<Property(MaxTest = 1)>]
    let ``format excel level 2 context`` () =
        [ z * (x + y), "=z*(x+y)"
          z * x / y, "=z*x/y"
          z / (x + y), "=z/(x+y)"
          z * max x y, "=z*MAX(x,y)"
          z * min x y, "=z*MIN(x,y)"
          z * x * y, "=z*x*y"
          z * x ** y, "=z*POW(x,y)"
          z * (x - y), "=z*(x-y)"
          z * (x % y), "=z*MOD(x,y)"
          z * (copysign x y), "=z*ABS(x)*SIGN(y)"
          z * (hypot x y), "=z*SQRT(POW(x,2)+POW(y,2))"
          z * Constant 2., "=2*z"
          z * Constant 0.5, "=0.5*z"
          z * abs x, "=z*ABS(x)"
          z * exp x, "=z*EXP(x)"
          z * log x, "=z*LOG(x)"
          z * -x, "=-z*x"
          z * sqrt x, "=z*SQRT(x)"
          z * cbrt x, "=z*POW(x,0.3333333333333333)"
          z * x, "=z*x"
          z * y, "=z*y" ]
        |> Seq.map (fun (expr, value) -> expr |> Expr.format Excel |> equals value)
        |> Seq.reduce (.&.)

    [<Property(MaxTest = 1)>]
    let ``format excel level 3 context`` () =
        [ abs (x + y), "=ABS(x+y)"
          abs (x / y), "=ABS(x/y)"
          abs (max x y), "=ABS(MAX(x,y))"
          abs (min x y), "=ABS(MIN(x,y))"
          abs (x * y), "=ABS(x*y)"
          abs (x ** y), "=ABS(POW(x,y))"
          abs (x - y), "=ABS(x-y)"
          abs (x % y), "=ABS(MOD(x,y))"
          abs (copysign x y), "=ABS(ABS(x)*SIGN(y))"
          abs (hypot x y), "=ABS(SQRT(POW(x,2)+POW(y,2)))"
          abs (Constant 2.), "=2"
          abs (Constant 0.5), "=0.5"
          abs (abs x), "=ABS(ABS(x))"
          abs (exp x), "=ABS(EXP(x))"
          abs (log x), "=ABS(LOG(x))"
          abs -x, "=ABS(-x)"
          abs (sqrt x), "=ABS(SQRT(x))"
          abs (cbrt x), "=ABS(POW(x,0.3333333333333333))"
          abs x, "=ABS(x)"
          abs y, "=ABS(y)" ]
        |> Seq.map (fun (expr, value) -> expr |> Expr.format Excel |> equals value)
        |> Seq.reduce (.&.)
