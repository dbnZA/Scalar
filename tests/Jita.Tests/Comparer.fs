// <copyright file="Comparer.fs" company="Jita Project">
// Copyright (c) Jita Project. All rights reserved.
// </copyright>
namespace Jita

open System.Collections.Generic
open FsCheck
open FsCheck.NUnit

module ComparerTests =
    let copy expr =
        Expr.traverseFast
            { new IExprVisitor<Expr> with
                member _.VisitBinaryFunc op e1 e2 = BinaryFunction(op, e1, e2)
                member _.VisitConstant c = Constant c
                member _.VisitProduct c s = Product(c, s)
                member _.VisitRoot n e = Root(n, e)
                member _.VisitSum c s = Sum(c, s)
                member _.VisitUnaryFunc op e = UnaryFunction(op, e)
                member _.VisitVariable v = Variable v }
            expr

    [<Property(Arbitrary = [| typeof<EvaluationGenerator> |], EndSize = 16, MaxTest = 1000)>]
    let ``copy equals`` eval =
        let copyExpr = copy eval.Expression
        let comparer = ExprComparer(eval.Expression, copyExpr) :> IEqualityComparer<Expr>

        ExprComparer.Equals(eval.Expression, eval.Expression) |@ "same equality"
        .&. (comparer.Equals(eval.Expression, eval.Expression) |@ "same equality")
        .&. (Expr.Equals(eval.Expression, copyExpr) |@ "copy equality")
        .&. (comparer.Equals(eval.Expression, copyExpr) |@ "copy equality")

    [<Property(Arbitrary = [| typeof<EvaluationGenerator> |], EndSize = 16, MaxTest = 1000)>]
    let ``not equals`` eval1 eval2 =
        let comparer =
            ExprComparer(eval1.Expression, eval2.Expression) :> IEqualityComparer<Expr>

        ((eval1.Expression = eval2.Expression) = ExprComparer.Equals(eval1.Expression, eval2.Expression)
         |@ "no equality")
        .&. ((eval1.Expression = eval2.Expression) = comparer.Equals(eval1.Expression, eval2.Expression)
             |@ "no equality")

    [<Property(Arbitrary = [| typeof<EvaluationGenerator> |], EndSize = 16, MaxTest = 1000)>]
    let ``copy hashcode`` eval =
        let copyExpr = copy eval.Expression
        let comparer = ExprComparer(eval.Expression, copyExpr) :> IEqualityComparer<Expr>

        (eval.Expression.GetHashCode() = eval.Expression.GetHashCode()
         |@ "same hash code")
        .&. (comparer.GetHashCode(eval.Expression) = comparer.GetHashCode(eval.Expression)
             |@ "same hash code")
        .&. (eval.Expression.GetHashCode() = copyExpr.GetHashCode() |@ "copy hash code")
        .&. (comparer.GetHashCode(eval.Expression) = comparer.GetHashCode(copyExpr)
             |@ "copy hash code")

    [<Property(MaxTest = 1)>]
    let ``traverse uses tail call`` () =
        let rec deepExpr i expr =
            if i = 0 then expr else deepExpr (i - 1) (abs expr)

        let expr1 = deepExpr 100000 Expr.Zero
        let expr2 = deepExpr 100000 Expr.Zero
        let comparer = ExprComparer(expr1, expr2) :> IEqualityComparer<Expr>
        ExprComparer.Equals(expr1, expr2) |> ignore
        comparer.Equals(expr1, expr2) |> ignore

    [<Property>]
    let ``cached equality`` eval =
        let copyExpr = copy eval.Expression
        let expr1 = eval.Expression + copyExpr
        let expr2 = eval.Expression + copyExpr
        let comparer = ExprComparer(expr1, expr2) :> IEqualityComparer<Expr>

        ExprComparer.Equals(expr1, expr2) |@ "expression equality"
        .&. (comparer.Equals(expr1, expr2) |@ "caching expression equality")
        .&. (comparer.Equals(expr1, expr2) |@ "caching equality")
