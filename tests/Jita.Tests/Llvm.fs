// <copyright file="Compile.fs" company="Jita Project">
// Copyright (c) Jita Project. All rights reserved.
// </copyright>
namespace Jita

open System
open FsCheck
open Jita.Math
open NUnit.Framework

module LlvmTests =
    [<TestCase(-2)>]
    [<TestCase(-1)>]
    [<TestCase(0)>]
    [<TestCase(1)>]
    [<TestCase(2)>]
    [<TestCase(3)>]
    [<TestCase(4)>]
    [<TestCase(5)>]
    let ``llvm rootn`` n =
        let exprFunc = [| rootn (Variable "x") n |] |> Expr.compileLlvm [| "x" |]

        for x in [| -infinity; -2.; -1.; -0.; nan; 0.; 1; 2; infinity |] do
            exprFunc.Input[0] <- x
            let output = exprFunc.Invoke()
            let expected = rootn x n

            Assert.That(
                BitConverter.DoubleToInt64Bits(output[0]),
                Is.EqualTo(BitConverter.DoubleToInt64Bits(expected)),
                $"{x}^(1/{n}): {expected} <> {output[0]}"
            )
