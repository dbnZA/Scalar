// <copyright file="Traverse.fs" company="Jita Project">
// Copyright (c) Jita Project. All rights reserved.
// </copyright>
namespace Jita

open FsCheck
open FsCheck.NUnit

module TraverseTests =
    [<Property(MaxTest = 1)>]
    let ``traverse uses tail call`` () =
        let rec deepExpr i expr =
            if i = 0 then expr else deepExpr (i - 1) (exp expr)

        deepExpr 100000 (Variable "x")
        |> Expr.traverseFast
            { new SimpleExprVisitor<int>() with
                member _.VisitBinaryFunc _ c1 c2 = c1 + c2 + 1
                member _.VisitConstant _ = 1
                member _.VisitRoot _ c = c + 1
                member _.VisitUnaryFunc _ c = c + 1
                member _.VisitVariable _ = 1 } = 100001
        |@ "Traverse failed to count correctly"
