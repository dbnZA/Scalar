// <copyright file="TestUtils.fs" company="Jita Project">
// Copyright (c) Jita Project. All rights reserved.
// </copyright>
namespace Jita

open System
open System.Collections.Concurrent
open System.Numerics
open FsCheck
open Jita.Math
open NUnit.Framework

[<assembly: Parallelizable(ParallelScope.Children)>]
do ()

[<AutoOpen>]
module TestUtils =
    let equals (expected: 'a) (actual: 'a) =
        expected.Equals(actual) |@ $"{expected} <> {actual}"

    let approx bits expected actual =
        if Double.IsFinite expected then
            let delta = abs expected * 2. ** (float <| -53 + bits)

            (expected + delta >= actual && expected - delta <= actual)
            |@ $"{expected} ± {delta} <> {actual}"
        else
            equals expected actual

    let approx32 bits expected actual =
        if Single.IsFinite expected then
            let delta = abs expected * 2.f ** (float32 <| -25 + bits)

            (expected + delta >= actual && expected - delta <= actual)
            |@ $"{expected} ± {delta} <> {actual}"
        else
            equals expected actual

    let same expected actual =
        match expected, actual with
        | BinaryAnnotation((e1, _), (e2, _)) -> e2 |> equals e1
        | _ -> Object.ReferenceEquals(expected, actual) |@ $"%A{expected} <> %A{actual}"

    let ``evaluates to`` expected =
        function
        | IsConstant c -> c |> equals expected
        | e -> false |@ $"{expected} <> {Expr.format FSharp e}"

    let toVector<'a when 'a: (new: unit -> 'a) and 'a: struct and 'a :> ValueType> =
        Array.map<'a, 'a Vector> Vector

    let msil symbols (exprs: Expr array) (values: _ array) =
        let exprFunc = exprs |> Expr.compileMsil symbols
        values.CopyTo(exprFunc.Input)
        exprFunc.Invoke().ToArray()

    let llvm symbols exprs (values: 'a array) =
        let exprFunc = exprs |> Expr.compileLlvm symbols
        values.CopyTo(exprFunc.Input)
        exprFunc.Invoke().ToArray()

    type Evaluation =
        { Expression: Expr
          Answer: float
          Variables: (string * float) list }

    type private InvertibleEvaluation =
        { InvertibleExpression: Expr Invertible
          Answer: float
          Variables: (string * float) list }

    let private evaluationGenerator =
        let genVariable =
            Gen.map2 (fun name value ->
                { Expression = Variable name
                  Answer = float value
                  Variables = [ name, float value ] })
            <| Arb.generate<string>
            <| Arb.generate<NormalFloat>

        let expr' expr' size =
            if size <= 0 then
                genVariable
            else
                Gen.oneof
                    [| genVariable
                       Gen.map
                           (fun (eval: Evaluation) ->
                               { Expression = Annotation(Constant eval.Answer, eval.Expression)
                                 Answer = eval.Answer
                                 Variables = eval.Variables })
                           genVariable
                       Gen.map2
                           (fun eval n ->
                               { Expression = Root(eval.Expression, n)
                                 Answer = rootn eval.Answer n
                                 Variables = eval.Variables })
                           (expr' (size / 2))
                           Arb.generate<int>
                       Gen.map3
                           (fun (c: NormalFloat) s (expr, op, opInvert) ->
                               { Expression = expr (c.Get, s |> List.map _.InvertibleExpression)
                                 Answer =
                                   s
                                   |> List.rev
                                   |> List.fold
                                       (fun c e ->
                                           (match e.InvertibleExpression with
                                            | Inverse _ -> opInvert
                                            | _ -> op)
                                               c
                                               e.Answer)
                                       c.Get
                                 Variables = s |> List.collect _.Variables })
                           Arb.generate<NormalFloat>
                           (Gen.listOf (
                               Gen.sized (fun size ->
                                   Gen.map2 (fun invert eval ->
                                       { InvertibleExpression =
                                           if invert then
                                               Inverse eval.Expression
                                           else
                                               NonInverse eval.Expression
                                         Answer = eval.Answer
                                         Variables = eval.Variables })
                                   <| Arb.generate<bool>
                                   <| expr' (size - 1))
                           ))
                           (Gen.elements [ Product, (*), (/); Sum, (+), (-) ])
                       Gen.map2
                           (fun eval (exprOp, op) ->
                               { Expression = UnaryFunction(exprOp, eval.Expression)
                                 Answer = op eval.Answer
                                 Variables = eval.Variables })
                           (expr' (size - 1))
                           (Gen.elements
                               [ Absolute, abs
                                 CubeRoot, cbrt
                                 Exponent, exp
                                 Logarithm, log
                                 Negate, (~-)
                                 SquareRoot, sqrt ])
                       Gen.map3
                           (fun eval1 eval2 (exprOp, op) ->
                               { Expression = BinaryFunction(exprOp, eval1.Expression, eval2.Expression)
                                 Answer = op eval1.Answer eval2.Answer
                                 Variables = eval1.Variables @ eval2.Variables })
                           (expr' (size / 2))
                           (expr' (size / 2))
                           (Gen.elements
                               [ Add, (+)
                                 CopySign, copysign
                                 Divide, (/)
                                 Hypotenuse, hypot
                                 Max, max
                                 Min, min
                                 Modulus, (%)
                                 Multiply, (*)
                                 Power, ( ** )
                                 Subtract, (-) ]) |]

        let cache = ConcurrentDictionary<_, _ Gen>()
        let rec cachedExpr' size = cache.GetOrAdd(size, expr' cachedExpr')

        Gen.sized cachedExpr'

    type EvaluationGenerator() =
        static member Evaluation() =
            evaluationGenerator
            |> Gen.filter (fun eval -> Double.IsFinite eval.Answer)
            |> Gen.filter (fun eval ->
                eval.Variables
                |> List.groupBy fst
                |> List.exists (fun v -> snd v |> List.length > 1)
                |> not)
            |> Arb.fromGen

    type EvaluationTwoGenerator() =
        static member Evaluation() =
            evaluationGenerator
            |> Gen.two
            |> Gen.filter (fun (a, b) -> Double.IsFinite a.Answer && Double.IsFinite b.Answer)
            |> Gen.filter (fun (a, b) ->
                a.Variables @ b.Variables
                |> List.groupBy fst
                |> List.exists (fun v -> snd v |> List.length > 1)
                |> not)
            |> Arb.fromGen

module Vector =
    let all<'a when 'a: (new: unit -> 'a) and 'a: struct and 'a :> ValueType and 'a: equality>
        (check: 'a -> 'a -> Property)
        expected
        (actual: 'a Vector)
        =
        seq { for i in 0 .. (Vector<'a>.Count - 1) -> actual[i] |> check expected |@ $"Vector.[{i}]" }
        |> Seq.reduce (.&.)
