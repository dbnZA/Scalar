// <copyright file="Evaluate.fs" company="Jita Project">
// Copyright (c) Jita Project. All rights reserved.
// </copyright>
namespace Jita

open System
open FsCheck
open FsCheck.NUnit
open Jita.Math

module EvaluateTests =
    [<Property>]
    let ``evaluate add with constants`` a b =
        Constant a + Constant b |> ``evaluates to`` (a + b)

    [<Property>]
    let ``evaluate cube root with constant`` a =
        cbrt (Constant a) |> ``evaluates to`` (cbrt a)

    [<Property>]
    let ``evaluate divide 0.`` a = Constant 0. / a |> ``evaluates to`` 0.

    [<Property>]
    let ``evaluate divide with constants`` a b =
        match a with
        | 0. -> Constant a / Constant b |> ``evaluates to`` 0.
        | _ -> Constant a / Constant b |> ``evaluates to`` (a / b)

    [<Property>]
    let ``evaluate hypotenuse with constants`` a b =
        hypot (Constant a) (Constant b) |> ``evaluates to`` (hypot a b)

    [<Property>]
    let ``evaluate hypotenuse with 0.`` a =
        hypot (Constant a) (Constant 0.) |> ``evaluates to`` (abs a)
        .&. (hypot (Constant 0.) (Constant a) |> ``evaluates to`` (abs a))

    [<Property>]
    let ``evaluate max with constants`` a b =
        max (Constant a) (Constant b) |> ``evaluates to`` (max a b)

    [<Property>]
    let ``evaluate min with constants`` a b =
        min (Constant a) (Constant b) |> ``evaluates to`` (min a b)

    [<Property>]
    let ``evaluate modulus with constants`` a b =
        Constant a % Constant b |> ``evaluates to`` (a % b)

    [<Property>]
    let ``evaluate multiply by 0.`` a =
        Constant 0. * a |> ``evaluates to`` 0.
        .&. (a * Constant 0. |> ``evaluates to`` 0.)

    [<Property>]
    let ``evaluate multiply with constants`` a b =
        match (a, b) with
        | 0., _
        | _, 0. -> Constant a * Constant b |> ``evaluates to`` 0.
        | _ -> Constant a * Constant b |> ``evaluates to`` (a * b)

    [<Property>]
    let ``evaluate power to 0.`` a = a ** Constant 0. |> ``evaluates to`` 1.

    [<Property>]
    let ``evaluate power with constant`` (a: NormalFloat) (b: NormalFloat) =
        Constant a.Get ** Constant b.Get |> ``evaluates to`` (a.Get ** b.Get)

    [<Property>]
    let ``evaluate subtract with constants`` a b =
        Constant a - Constant b |> ``evaluates to`` (a - b)

    [<Property>]
    let ``evaluate absolute with constant`` a =
        abs (Constant a) |> ``evaluates to`` (abs a)

    [<Property>]
    let ``evaluate exponent with constant`` a =
        exp (Constant a) |> ``evaluates to`` (exp a)

    [<Property>]
    let ``evaluate logarithm with constant`` a =
        log (Constant a) |> ``evaluates to`` (log a)

    [<Property>]
    let ``evaluate negate with constant`` a = -(Constant a) |> ``evaluates to`` -a

    [<Property>]
    let ``evaluate square root with constant`` a =
        sqrt (Constant a) |> ``evaluates to`` (sqrt a)

    let evaluateTreeFloat32 variables expr =
        expr
        |> Expr.traverseFast
            { new SimpleExprVisitor<float32>() with
                member _.VisitBinaryFunc op v1 v2 =
                    match op with
                    | Add -> v1 + v2
                    | CopySign -> Single.CopySign(v1, v2)
                    | Divide -> v1 / v2
                    | Hypotenuse -> Single.Hypot(v1, v2)
                    | Max -> max v1 v2
                    | Min -> min v1 v2
                    | Modulus -> v1 % v2
                    | Multiply -> v1 * v2
                    | Power -> v1 ** v2
                    | Subtract -> v1 - v2

                member _.VisitConstant value = float32 value

                member _.VisitRoot value n = Single.RootN(value, n)

                member _.VisitUnaryFunc op value =
                    match op with
                    | Absolute -> abs value
                    | CubeRoot -> cbrt value
                    | Exponent -> exp value
                    | Logarithm -> log value
                    | Negate -> -value
                    | SquareRoot -> sqrt value

                member _.VisitVariable name = variables |> Map.find name |> float32 }

    [<Property(Arbitrary = [| typeof<EvaluationGenerator> |], EndSize = 16, MaxTest = 1_000)>]
    let ``evaluate expression`` eval =
        let symbols, values = eval.Variables |> List.toArray |> Array.unzip
        let values32 = values |> Array.map float32
        let answer32 = eval.Expression |> evaluateTreeFloat32 (eval.Variables |> Map.ofList)

        match eval.Expression with
        | Annotation(_, expr) ->
            expr |> Expr.evaluate (eval.Variables |> Map.ofSeq) |> equals eval.Answer
            .&. (expr.Evaluate(eval.Variables) |> equals eval.Answer)
        | _ -> true |@ "non-annotation"
        .&. (eval.Expression
             |> Expr.evaluate (eval.Variables |> Map.ofSeq)
             |> equals eval.Answer
             |@ "Expr.evaluate")
        .&. (eval.Expression.Evaluate(eval.Variables) |> equals eval.Answer |@ "Evaluate")
        .&. (symbols.Length > 0
             ==> lazy
                 ([| eval.Expression |] |> msil symbols <| values
                  |> Array.item 0
                  |> equals eval.Answer
                  |@ "Msil float"))
        .&. (symbols.Length > 0
             ==> lazy
                 ([| eval.Expression |] |> msil symbols <| values32
                  |> Array.item 0
                  |> approx32 4 answer32
                  |@ "Msil float32"))
        .&. (symbols.Length > 0
             ==> lazy
                 ([| eval.Expression |] |> msil symbols <| toVector values
                  |> Array.item 0
                  |> Vector.all equals eval.Answer
                  |@ "Msil float Vector"))
        .&. (symbols.Length > 0
             ==> lazy
                 ([| eval.Expression |] |> msil symbols <| toVector values32
                  |> Array.item 0
                  |> Vector.all (approx32 3) answer32
                  |@ "Msil float32 Vector"))
        .&. (symbols.Length > 0
             ==> lazy
                 ([| eval.Expression |] |> llvm symbols <| values
                  |> Array.item 0
                  |> approx 4 eval.Answer
                  |@ "LLVM float"))
        .&. (symbols.Length > 0
             ==> lazy
                 ([| eval.Expression |] |> llvm symbols <| values32
                  |> Array.item 0
                  |> approx32 7 answer32
                  |@ "LLVM float32"))
        .&. (symbols.Length > 0
             ==> lazy
                 ([| eval.Expression |] |> llvm symbols <| toVector values
                  |> Array.item 0
                  |> Vector.all (approx 4) eval.Answer
                  |@ "LLVM float Vector"))
        .&. (symbols.Length > 0
             ==> lazy
                 ([| eval.Expression |] |> llvm symbols <| toVector values32
                  |> Array.item 0
                  |> Vector.all (approx32 7) answer32
                  |@ "LLVM float32 Vector"))
