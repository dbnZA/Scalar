# Just In Time Arithmetic (Jita)
[![pipeline status](https://gitlab.com/dbnZA/Jita/badges/master/pipeline.svg)](https://gitlab.com/dbnZA/Jita/pipelines)
[![coverage report](https://gitlab.com/dbnZA/Jita/badges/master/coverage.svg)](https://dbnza.gitlab.io/Jita/coverage/index.htm)

Generic scalar type for .NET.

## Development
- Restore the required build tools: `dotnet tool restore`
- Ensure source files are properly formatted prior to committing ([link](https://fsprojects.github.io/fantomas/docs/end-users/GitHooks.html)): 
```shell
cat > .git/hooks/pre-commit << __EOF__
#!/bin/sh
git diff --cached --name-only --diff-filter=ACM -z | xargs -0 dotnet fantomas
git diff --cached --name-only --diff-filter=ACM -z | xargs -0 git add
__EOF__
chmod a+x .git/hooks/pre-commit 
```
